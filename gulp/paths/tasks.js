'use strict'

module.exports = [
  './gulp/tasks/scss.js',
  // './gulp/tasks/pug.js',
  './gulp/tasks/html.js',
  './gulp/tasks/scripts.js',
  './gulp/tasks/image.js',
  './gulp/tasks/clean.js',
  './gulp/tasks/serve.js',
  './gulp/tasks/watch.js',
];
