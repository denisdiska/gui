'use strict'

module.exports = function () {
  $.gulp.task('scripts', function () {
    return $.gulp.src([
      './source/libs/jquery/jquery.min.js',
      './source/libs/bootstrap/bootstrap.min.js',
      './source/libs/owl-carousel-2/owl.carousel.min.js',
      './source/libs/slick/slick.min.js',
      './source/js/common.js' // Always at the end
    ])
    .pipe($.concat('scripts.min.js'))
    .pipe($.uglify()) // Mifify js (opt.)
    .on('error', $.gp.notify.onError(function(){
      return {
        title: 'Scripts',
      }
    }))
    .pipe($.gulp.dest('./build/assets/js'))
    });
}