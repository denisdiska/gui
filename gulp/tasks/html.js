'use strict'


module.exports = function () {
    $.gulp.task('html', function () {
      return $.gulp.src('./source/pages/*.html')
      .pipe($.gp.pug({ pretty: true }))
      .on('error', $.gp.notify.onError(function(error){
        return {
          title: 'html',
          message: error.message
        }
      }))
      .pipe($.gulp.dest('./build/'))
      });
  }