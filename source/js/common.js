jQuery(document).ready(function($) { 

// === OWL CAROUSEL
    $("#carousel_1").owlCarousel({
        loop: true,
        nav: true,
        dots: false,
        center: true,
        navText: ['<img class="owl-prev__img" src="../assets/img/nav_items/switch_right.png">', '<img class="owl-next__img" src="../assets/img/nav_items/switch_right.png">'],
        
        responsive:{
            0:{
                items:1,
                nav: true
            }
          }
      });


// === SLICK SLIDER

$('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  infinite: true,
  asNavFor: '.slider-nav',
  
});
$('.slider-nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: true,
  centerMode: true,
  infinite: true,
  focusOnSelect: true,
  prevArrow: "<img src='./assets/img/icons/prev-arrow.png' class='prev prev-bcg' alt='1'>",
  nextArrow: "<img src='./assets/img/icons/next-arrow.png' class='next next-bcg' alt='1'>",
  responsive: [
    {
      breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
    },
    {
      breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
    },
  ]
});




// === TAB_TRIGGER
    
    $('.js-tab-trigger').click(function() {    
    
    
    var id = $(this).attr('data-tab'),
    content = $(this).parents(".tabs").find('.js-tab-content[data-tab="' + id +'"]');
    
    $(this).parents(".tabs").find('.js-tab-trigger.active').removeClass('active');
    $(this).addClass('active');
    
    $(this).parents(".tabs").find('.js-tab-content.active').removeClass('active');
    content.addClass('active');
    });   



    // === SCROLL page 

    $('a[href*="#"]').on('click', function (e) {
        e.preventDefault();
       
        $('html, body').animate({
          scrollTop: $($(this).attr('href')).offset().top
        }, 500, 'linear');
      });


});



// === PROGRESS BAR

function animateProgressBar(el, width){
  el.animate(
      {width: width}, 
      {
          duration: 2000,
          step: function( fx) {
              if(fx.prop == 'width') {
                  el.html(el.data("name") );
              }
          }
      }        
  );    
}


$('.progress').each(function(){
 animateProgressBar($(this).find("div"), $(this).data("width")) 
});










